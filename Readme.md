# gitBack

## What

GIT commit and push, able to run via cron.

Tested with Ubuntu 18.10

Bugs? Yes.

## Why

There are at least a million backup utilities available but I have never found one that does exactly what I want. So I wrote my own.

## Assumptions

- You have local GIT repositories you want to push to another location for backup
- Your system is capable of sending mail from the command line (`$ echo test | mail -s testing you@you.com`)
- You are configuring for personal/private backup repositories -- not a shared development repo

## How

### Server Setup

#### Quick Start

Notes: 

- You may first need to add `/usr/bin/git-shell` to `/etc/shells`
- You should create a unique gitUser name for each user and restrict access via `~gitUser/.ssh/authorized_keys`
- You could create a shared gitUser with these same commands and then add each real users SSH key to `~gitUser/.ssh/authorized_keys`

```
# adduser gitUser --home /path/to/repos/gitUser --shell /usr/bin/git-shell --comment "git server"
# sudo -u gitUser mkdir .ssh 
# sudo -u gitUser chmod 700 .ssh
# sudo -u gitUser touch .ssh/authorized_keys 
# sudo -u gitUser chmod 600 .ssh/authorized_keys
```
#### Full Details
[GIT Manual](https://git-scm.com/book/en/v2/Git-on-the-Server-Setting-Up-the-Server)

### User Setup

#### Add gitBack to Users System

- Put gitBack in any accessible location
- Put .gitBack in gitUser's home directory

#### 	Create Key


 **Protect This Key!!!**

```
	$ ssh-keygen -t rsa -b 4096 -f ~/.ssh/git_id_rsa
```
If you want to use the key from a cron job do not set a passphrase (just hit return).

Now you (or the gitBack server admin) must add the contents of `~/.ssh/git_id_rsa.pub` to `~gitUser/.ssh/authorized_keys` on the git server
		

#### Create `.ssh/config`
```
$ cat <<EOF >>~/.ssh/config
Host gitBack
	User gitUser
	HostName REALHOST
	IdentityFile ~/.ssh/git_id_rsa
	IdentitiesOnly yes
EOF
$ chmod 600 ~/.ssh/config
```
This will allow you to use "gitBack" as your host name for git commands. It will map to the correct REALHOST using your specific ssh key.

### Add a New Repository:

Assuming local folder NEWGIT is already a GIT repository.

```
$ cd NEWGIT
$ git init --bare gitUser@gitBack:~gitUser/NEWGIT.git
$ git remote add backup gitUser@gitBack:~gitUser/NEWGIT.git
$ git push --set-upstream backup master
$ vi ~/.gitBack
  Add NEWGIT to the GITBACK array in  ~/.gitBack
  Update ~/.gitBack with your correct email address.
```

### Usage

#### Command Line
```
$ gitBack
```
You should see no output. The logfile will be sent to the email configured in ~/.gitBack.

#### Crontab (example to run everyday at 4:15am)
```
$ crontab -e
15 04 * * * /full/path/to/gitBack
```
